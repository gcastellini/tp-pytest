from pages import LoginPage as page
from core.steps.BaseSteps import BaseStep
from core.assertion.Assertion import Assertion


class LoginSteps(BaseStep):

    def pageLogoDisplayed(self):
        Assertion.assertTrue('Logo is not displayed', page.getPageLogo().isDisplayed())
        return self

    def clickAccount(self):
        elem = page.getAccountButton()
        elem.click()
        return self

    def insertUsername(self, text):
        Assertion.assertTrue('No se muestra Username input', page.getUsername().isDisplayed())
        elem = page.getUsername()
        elem.setText(text)
        return self

    def insertPassword(self, text):
        Assertion.assertTrue('No se muestra Password input', page.getPassword().isDisplayed())
        elem = page.getPassword()
        elem.setText(text)
        return self

    def clickLogin(self):
        Assertion.assertTrue('No se muestra botón Login', page.getLogin().isDisplayed())
        elem = page.getLogin()
        elem.click()
        return self

    def showWelcome(self):
        Assertion.assertEquals('No se muestra mensaje de login',
                               "Hello giuliana_castellini (not giuliana_castellini? Sign out)",
                               page.welcomeMessage().getText())
        return self

    def errorMessage(self):
        Assertion.assertEquals('No se muestra mensaje de error',
                               "Error: A user could not be found with this email address.",
                               page.errorMessage().getText())
        return self
