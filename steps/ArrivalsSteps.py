from pages import ArrivalsPage as page
from core.steps.BaseSteps import BaseStep
from core.assertion.Assertion import Assertion

class ArrivalsSteps(BaseStep):

 def pageLogoDisplayed(self):
  Assertion.assertTrue('Logo is not displayed', page.getPageLogo().isDisplayed())
  return self

 def clickShop(self):
  elem = page.getShopButton()
  elem.click()
  return self

 def clickHome(self):
  elem = page.getHomeButton()
  elem.click()
  return self


 def arrivalsItems(self):
  page.getArrivals()
  page.not4Arrivals()
  return self
