from pytest_bdd import then,given,when, scenarios
from steps.ArrivalsSteps import ArrivalsSteps as arrivals

scenarios('../features/Arrivals.feature')

@given("The client is in Practice page")
def logoDisplayed():
    arrivals().pageLogoDisplayed()

@when("The client clicks on Shop Button")
def clickShop():
    arrivals().clickShop()

@when("The client clicks on Home menu button")
def clickHome():
    arrivals().clickHome()

@then("The client verify that results are shown properly")
def checkResults():
    arrivals().arrivalsItems()

def teardown():
    arrivals().closeBrowser()