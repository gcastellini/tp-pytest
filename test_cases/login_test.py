from pytest_bdd import then, given, when, scenarios
from steps.LoginSteps import LoginSteps as login

scenarios('../features/Login.feature')


@given("The client is in Practice page")
def logoDisplayed():
    login().pageLogoDisplayed()


@given("The client clicks on My Account button")
def clickAccount():
    login().clickAccount()


@when("The client enters <email>")
def getinsertUser(email):
    login().insertUsername(email)


@when("The client enters <password>")
def getinsertPass(password):
    login().insertPassword(password)


@when("The client clicks on login button")
def clickLogin():
    login().clickLogin()

@then("The client is logged in successfully")
def checkResult():
    login().showWelcome()

@then("The client sees error message")
def checkError():
    login().errorMessage()

def teardown():
    login().closeBrowser()