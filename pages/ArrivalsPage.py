from core.ui.WebUIElement import WebUIElement as UIElement
from core.ui.By import By
from core.assertion.Assertion import Assertion


def getPageLogo():
    return UIElement(By.ID, 'site-logo')


def getShopButton():
    return UIElement(By.ID, "menu-item-40")


def getHomeButton():
    return UIElement(By.XPATH, "//*[@id='content']/nav/a")


def getArrivals():
    string = "//*[@id='themify_builder_content-22']/div[2]/div/div/div/div/div[2]/div["
    for i in range(1, 4):
       Assertion.assertTrue('No existen 3 elementos',UIElement(By.XPATH,string+str(i)+"]").isDisplayed())


def not4Arrivals():
    string = "//*[@id='themify_builder_content-22']/div[2]/div/div/div/div/div[2]/div["
    Assertion.assertFalse("Hay un 4 elemento",UIElement(By.XPATH,string+"4]").isDisplayed())