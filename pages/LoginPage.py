from core.ui.WebUIElement import WebUIElement as UIElement
from core.ui.By import By

def getPageLogo():
    return UIElement(By.ID, 'site-logo')

def getAccountButton():
    return UIElement(By.ID, "menu-item-50")

def getUsername():
    return UIElement(By.ID, "username")

def getPassword():
    return UIElement(By.ID, "password")

def getLogin():
    return UIElement(By.NAME,"login")

def welcomeMessage():
    return UIElement(By.XPATH,"//*[@id='page-36']/div/div[1]/div/p[1]")

def errorMessage():
    return UIElement(By.XPATH,"//*[@id='page-36']/div/div[1]/ul/li")